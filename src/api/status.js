/* eslint-disable import/no-anonymous-default-export */

const getAllData = async (API_NAME) => {
  const API_URL = `https://api.factoryfour.com/${API_NAME}/health/status`;
  const res = await fetch(API_URL);
  const data = await res.json();
  return data;
};

export default {
  getAllData,
};
