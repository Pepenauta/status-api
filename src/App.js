import AppBar from "./components/AppBar";
import Card from "./components/Card";

import apiNames from "./utils/constants";

import "bulma/css/bulma.min.css";

function App() {
  return (
    <>
      <AppBar title='Status Dashboard' />
      <div className='container'>
        <div className='columns is-multiline mt-2'>
          {apiNames.map((apiName) => (
            <div
              key={apiName}
              className='column is-12-mobile is-6-tablet is-3-desktop'
            >
              <Card apiName={apiName} />
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

export default App;
