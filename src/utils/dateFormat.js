const dateFormat = (dateTime) => {
  let timeAPI = new Date(dateTime);
  //console.log(timeAPI);
  let timeFormat = timeAPI.toLocaleString(navigator.language, {
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  });
  return timeFormat;
};

export default dateFormat;
