import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders title bar", () => {
  render(<App />);
  const titleBar = screen.getByText("Status Dashboard");
  expect(titleBar).toBeInTheDocument();
});
