function AppBar({ title }) {
  return (
    <header className='navbar is-dark'>
      <div className='navbar-brand'>
        <div className='navbar-item'>{title}</div>
      </div>
    </header>
  );
}

export default AppBar;
