<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">Status API Page</h3>
  [Project Demo](https://frolicking-licorice-e140cf.netlify.app/)
</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>

  </ol>
</details>


<!-- ABOUT THE PROJECT -->
## About The Project

<div align="center">
  <a href="https://frolicking-licorice-e140cf.netlify.app/" title="demo page on netifly" target="_blank">
    <img src="/screenshot.project.png" alt="Project Screen Shot" width="75%" height="auto" >
  </a>
</div>

A status page for the FactoryFour APIs. A single-page web application written
using React.


<p align="right">(<a href="#top">back to top</a>)</p>


### Built With

* [React.js](https://reactjs.org/)
* [Bulma](https://bulma.io/)

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites


* npm
  ```sh
  npm install npm@latest -g
  ```


### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/Pepenauta/status-api.git
   ```
2. Go to the project folder and install NPM packages
   ```sh
   cd status-api
   yarn
   ```
3. Run the project
   ```sh
   yarn start
   ```
4. Run the tests
   ```sh
   yarn test
   ```
4. You can chenge the refresh time, editing `utils/intervalTime.js`
   ```sh
    const TIME_REFRESH = 15000 //in millisenconds
   ```

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

José Espinosa - [@pepenauta](https://twitter.com/pepenauta) - pepenauta@gmail.com


<p align="right">(<a href="#top">back to top</a>)</p>
