import { useState, useEffect } from "react";
import statusAPI from "../api/status";

import dateFormat from "../utils/dateFormat";
import TIME_REFRESH from "../utils/intervalTime";

const Card = ({ apiName }) => {
  const [infoAPI, setInfoAPI] = useState([]);

  useEffect(() => {
    function dataAPI() {
      statusAPI.getAllData(apiName).then((data) => {
        setInfoAPI(data);
      });
    }
    dataAPI();
    const interval = setInterval(() => dataAPI(), TIME_REFRESH);
    return () => {
      clearInterval(interval);
    };
  }, [apiName]);

  return (
    <div className='card'>
      <div className='card-content'>
        {infoAPI.success ? (
          <div className='notification is-success'>
            <h2 className='is-uppercase has-text-weight-bold'>{apiName}</h2>
            <h3>Healthy</h3>
            <p className='is-size-7'>{infoAPI.hostname}</p>
            <p className='is-size-7'>{dateFormat(infoAPI.time)}</p>
          </div>
        ) : (
          <div className='notification is-danger'>
            <h2 className='is-uppercase has-text-weight-bold'>{apiName}</h2>
            <h3>Error</h3>
            <p className='is-size-7'>Out of Service 503</p>
            <p className='is-size-7'>Service Unavailable</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default Card;
